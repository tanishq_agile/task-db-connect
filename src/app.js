const feathers = require('@feathersjs/feathers');
const express = require('@feathersjs/express');
const socketio = require('@feathersjs/socketio');
const rest = require('@feathersjs/rest-client');
//const Sequelize = require('sequelize');

const mongoose = require('mongoose');
const service = require('feathers-mongoose');

const Model = require('./user-model');

mongoose.Promise = global.Promise;

// Connect to your MongoDB instance(s)
mongoose.connect('mongodb://localhost:27017/User');



// Create an Express compatible Feathers application instance.
const app = express(feathers());


// Connect to the same as the browser URL (only in the browser)
const restClient = rest();

// Connect to a different URL
const restClient = rest('http://feathers-api.com')

// Configure an AJAX library (see below) with that client 
app.configure(restClient.fetch(window.fetch));

// Connect to the `http://feathers-api.com/messages` service
const messages = app.service('messages');



// Turn on JSON parser for REST services
app.use(express.json());
// Turn on URL-encoded parser for REST services
app.use(express.urlencoded({extended: true}));
// Enable REST services
app.configure(express.rest());
// Enable Socket.io services
app.configure(socketio());
// Connect to the db, create and register a Feathers service.
app.use('/users', service({
  Model,
  lean: false, 
}));
app.use(express.errorHandler());



const getItems = {
  find(params) {
    let items = Model.find({});
    return Promise.resolve(items);
  },
  create(data, params) {},
  get(id, params) {},
  update(id, data, params) {},
  remove(id, params) {},
}



// Create a dummy Message
app.service('users').create({}).then(function(message) {
  console.log('Created User', message);
});

app.service('users').find({}).then(function(message) {
  console.log('Created Users list', message);
});


// Start the server.
const port = 3030;
app.listen(port, () => {
    console.log(`Feathers server listening on port ${port}`);
});