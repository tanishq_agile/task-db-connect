const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const UserSchema = new Schema({
    name: String,
    age: Number,

});
const Model = mongoose.model('User', UserSchema);

module.exports = Model;